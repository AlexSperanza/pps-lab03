package lab

import u02.Modules.Person
import u02.Modules.Person.Teacher
import u02.Optionals._
import u03.Lists.List
import u03.Lists.List.{Cons, Nil, append}

import scala.annotation.tailrec

object ListExtension {
  // 1.a
  @tailrec
  def drop[A](list: List[A], n: Int): List[A] = (list, n) match {
    case (Cons(_, t), n) if n > 0 => drop(t, n-1)
    case (_, n) if n == 0 => list
    case (Nil(), _) => Nil()
  }

  // 1.b
  def flatMap[A, B](list: List[A])(f: A => List[B]): List[B] = list match {
    case Cons(v, t) => append(f(v), flatMap(t)(f))
    case Nil() => Nil()
  }

  // 1.c
  def map[A,B](l: List[A])(mapper: A=>B): List[B] = flatMap(l)(v => Cons(mapper(v), Nil()))

  // 1.d
  def filter[A](list: List[A])(pred: A=>Boolean): List[A] = flatMap(list)({
    case v if pred(v) => Cons(v, Nil())
    case _ => Nil()
  })

  // 2
  def max(l: List[Int]): Option[Int] = {
    @tailrec
    def _max(subList: List[Int], currentMax: Option[Int]): Option[Int] = subList match {
      case Cons(h, t) if h >= Option.getOrElse(currentMax, h) => _max(t, Option.Some(h))
      case Cons(_, t) => _max(t, currentMax)
      case Nil() => currentMax
    }
    _max(l, Option.None())
  }

  // 3
  object PersonList {
    def getCourses(people: List[Person]): List[String] =
      // Without flatMap:
      // map(filter(people) { case _: Teacher => true; case _ => false })({ case Teacher(_, course) => course })
      flatMap(people)({case Teacher(_, course) => Cons(course, Nil()); case _ => Nil() })
  }

  // 4
  @tailrec
  def foldLeft[A](list: List[A])(initialValue: A)(reducer: (A, A) => A): A = list match {
    case Cons(head, tail) =>foldLeft(tail)(reducer(initialValue, head))(reducer)
    case Nil() => initialValue
  }

  def foldRight[A](list: List[A])(initialValue: A)(reducer: (A, A) => A): A = list match {
    case Cons(head, tail) => reducer(head, foldRight(tail)(initialValue)(reducer))
    case Nil() => initialValue
  }
}
