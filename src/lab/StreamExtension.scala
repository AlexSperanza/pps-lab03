package lab

import u03.Streams.Stream
import u03.Streams.Stream.cons

object StreamExtension {

  // 6
  def constant[A](value: A): Stream[A] = cons(value, constant(value))

  // 7
  def fibonacciStream(): Stream[Int] = {
    def _fibs(oldKnownValue: Int, lastKnownValue: Int): Stream[Int] = {
      val newValue = oldKnownValue + lastKnownValue
      cons(newValue, _fibs(lastKnownValue, newValue))
    }
    // 0 + lastInitValue have to result into 1
    val lastInitValue = 1
    // oldInitValue + lastInitValue must result in 0
    val oldInitValue = -1

    _fibs(oldInitValue, lastInitValue)
  }
}
