package lab

import u03.Lists._
import lab.ListExtension._
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.{BeforeEach, Test}
import u02.Modules.Person
import u03.Lists.List.Cons
import u03.Lists.List.Nil
import u02.Optionals.Option

class ListExtensionTest {

  var list: List[Int] = null

  @BeforeEach
  def beforeEach(): Unit = {
    list = Cons(0, Cons(1, Cons(2, Cons(3, Nil()))))
  }

  @Test
  def testNoDropList(): Unit = {
    val droppedList = drop(list, 0)
    assertEquals(list, droppedList)
  }

  @Test
  def testDropList(): Unit = {
    val droppedList = drop(list, 1)
    assertEquals(Cons(1, Cons(2, Cons(3, Nil()))), droppedList)
  }

  @Test
  def testTooMuchDropList(): Unit = {
    val droppedList = drop(list, 10)
    assertEquals(Nil(), droppedList)
  }

  @Test
  def testFlatMap(): Unit = {
    val flatMapList = flatMap(list)(v => Cons(v+1, Nil()))
    val expectedList = Cons(1, Cons(2, Cons(3, Cons(4, Nil()))))
    assertEquals(expectedList, flatMapList)
  }

  @Test
  def testDoubleFlatMap(): Unit = {
    val flatMapList = flatMap(list)(v => Cons(v+1, Cons(v + 2, Nil())))
    val expectedList = Cons(1, Cons(2, Cons(2, Cons(3, Cons(3, Cons(4, Cons(4, Cons(5, Nil()))))))))
    assertEquals(expectedList, flatMapList)
  }

  @Test
  def testMapWithFlatMap(): Unit = {
    val mappedList = map(list)(_*10)
    val expectedList = Cons(0, Cons(10, Cons(20, Cons(30, Nil()))))
    assertEquals(expectedList, mappedList)
  }

  @Test
  def testFilterWithFlatMap(): Unit = {
    val filteredList = filter(list)(_%2==0)
    val expectedList = Cons(0, Cons(2, Nil()))
    assertEquals(expectedList, filteredList)
  }

  @Test
  def testMax(): Unit = {
    val maxValue = max(list)
    val expectedMax = Option.Some(3);
    assertEquals(expectedMax, maxValue)
  }

  @Test
  def testNilMax(): Unit = {
    val maxValue = max(Nil())
    val expectedValue = Option.None()
    assertEquals(expectedValue, maxValue)
  }

  @Test
  def testTeacherCourses(): Unit = {
    val peopleList:List[Person] = Cons(Person.Student("Mario Rossi", 1),
                      Cons(Person.Teacher("Luca Bianchi", "Spam detection"),
                       Cons(Person.Student("Marco Neri", 3),
                        Nil())))
    val expectedCoursesList = Cons("Spam detection", Nil())
    assertEquals(expectedCoursesList, PersonList.getCourses(peopleList))
  }

  @Test
  def testNoTeacherCourse(): Unit = {
    val peopleList:List[Person] = Cons(Person.Student("Mario Rossi", 1),
        Cons(Person.Student("Marco Neri", 3),
          Nil()))
    val expectedCoursesList = Nil()
    assertEquals(expectedCoursesList, PersonList.getCourses(peopleList))
  }

  @Test
  def testFoldLeft(): Unit = {
    val expectedFoldedValue = 6
    assertEquals(expectedFoldedValue, foldLeft(list)(0)(_+_))
  }

  @Test
  def testFoldLeftOnEmptyList(): Unit = {
    val emptyList:List[Int] = Nil()
    val initialValue = 0
    assertEquals(initialValue, foldLeft(emptyList)(initialValue)(_-_))
  }

  @Test
  def testFoldLeftOnStringList(): Unit = {
    val stringList = Cons("Hello", Cons(", ", Cons("world", Cons("!", Nil()))))
    val expectedString = "Hello, world!"
    assertEquals(expectedString, foldLeft(stringList)("")(_+_))
  }

  @Test
  def testFoldRight(): Unit = {
    val initialValue = 0
    val expectedValue = -2
    assertEquals(expectedValue, foldRight(list)(initialValue)(_-_))
  }
}
