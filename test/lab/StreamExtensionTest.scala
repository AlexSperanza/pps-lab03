package lab

import lab.StreamExtension.constant
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import u03.Lists.List.{Cons, Nil}
import u03.Streams.Stream

class StreamExtensionTest {

  @Test
  def testDropN(): Unit = {
    val initialStream = Stream.take(Stream.iterate(0)(_+1))(10)
    val droppedStream = Stream.toList(Stream.drop(initialStream)(6))
    assertEquals(Cons (6 , Cons (7 , Cons (8 , Cons (9 , Nil() ) ) ) ), droppedStream)
  }

  @Test
  def testDropTooMuch(): Unit = {
    val initialStream = Stream.take(Stream.iterate(0)(_+1))(5)
    val droppedStream = Stream.toList(Stream.drop(initialStream)(6))
    assertEquals(Nil(), droppedStream)
  }

  @Test
  def testConstantIntStream(): Unit = {
    val constantStream = constant(2);
    val streamList = Stream.toList(Stream.take(constantStream)(4))
    val expectedList = Cons(2, Cons(2, Cons(2, Cons(2, Nil()))))
    assertEquals(expectedList, streamList)
  }

  @Test
  def testConstantStringStream(): Unit = {
    val constantStream = constant("a")
    val streamList = Stream.toList(Stream.take(constantStream)(5))
    val foldedStreamList = ListExtension.foldLeft(streamList)("")(_+_)
    val expectedString = "aaaaa"
    assertEquals(expectedString, foldedStreamList)
  }

  @Test
  def testFibonacciStream(): Unit = {
    val fibonacciStream = StreamExtension.fibonacciStream()
    val fibStreamList = Stream.toList(Stream.take(fibonacciStream)(8))
    val expectedStreamList =  Cons(0, Cons(1, Cons(1, Cons(2, Cons(3, Cons(5, Cons(8, Cons(13, Nil()))))))))
    assertEquals(expectedStreamList, fibStreamList)
  }

  @Test
  def testNthFibonacciNumber(): Unit = {
    val taken = 20
    val dropped = taken - 1
    val fibonacciStream = StreamExtension.fibonacciStream()
    val fibStreamList = Stream.toList(Stream.take(fibonacciStream)(taken))
    val nthFibonacciNumber = ListExtension.drop(fibStreamList, dropped)
    val expectedList = Cons(4181, Nil())
    assertEquals(nthFibonacciNumber, expectedList)
  }

}
